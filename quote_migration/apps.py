from django.apps import AppConfig


class QuoteMigrationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'quote_migration'
