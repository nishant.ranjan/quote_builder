import json

import requests
from django.conf import settings

from quotes_app.models import Quotes


class Quote:
    @staticmethod
    def process():
        raise NotImplementedError


class QuoteMigrate(Quote):
    quote_api_url = settings.QUOTE_API_URL

    def process(self):
        try:
            response = requests.request("GET", self.quote_api_url, headers={}, data={})
            filtered_quotes = self.check_quotes(json.loads(response.text)[100:200])
            self._insert_into_db(filtered_quotes)
        except Exception as e:
            print(e)

    @staticmethod
    def check_quotes(quotes):
        quote_data = []
        for quote in quotes:
            if 0 < len(quote.get("text").split()) <= 5:
                quote.update({"tag": "short"})
            elif 5 < len(quote.get("text").split()) <= 10:
                quote.update({"tag": "mid"})
            else:
                quote.update({"tag": "long"})
            quote_data.append(quote)
        return quote_data

    @staticmethod
    def _insert_into_db(filtered_quotes):
        for quotes in filtered_quotes:
            Quotes.objects.create(quote=quotes['text'], tag=quotes['tag'])


class QuoteRemove(Quote):
    def process(self):
        pass
