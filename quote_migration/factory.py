from quote_migration.views import QuoteMigrate, QuoteRemove

action_dict = {
    "insert_data": QuoteMigrate,
    "delete_data": QuoteRemove
}


def get_factory_class(key):
    return action_dict[key]
