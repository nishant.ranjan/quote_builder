from django.core.management.base import BaseCommand, CommandError

from quote_migration.factory import get_factory_class


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('action', type=str)

    def handle(self, *args, **options):
        try:
            action_class = get_factory_class(options["action"])
            quote_obj = action_class()
            quote_obj.process()
        except KeyError as e:
            raise CommandError("invalid argument : only insert_data and delete_data are allowed "
                               "in action", e)
