import datetime
import hashlib
import json
import uuid
from unittest.mock import patch

from rest_framework import status
from rest_framework.test import APIClient
from django.conf import settings
from django.test import TestCase

from quotes_app.models import UserBase
from quotes_app.views.user_register_view import UserRegister


class ValidateUserData(TestCase):
    def setUp(self) -> None:
        self.obj = UserRegister()
        secret_key = str(uuid.uuid4())
        expiry_date_time = datetime.datetime.now() + datetime.timedelta(
            minutes=settings.TOKEN_EXPIRY_DURATION)
        UserBase.objects.create(
            username="test", email="test@test.com",
            password=hashlib.sha512("12345678".encode('utf-8')).hexdigest(),
            tag=str(["short", "mid"]),
            token_expiry_date=expiry_date_time,
            secret_key=secret_key
        )

    def test_1(self):
        """
        test case when all the data is correct
        """
        post_data = {
            "username": "testingalphaaa",
            "email": "nishant9233@test5.com",
            "password": "nishantran123",
            "confirm_password": "nishantran123",
            "tag": ["short", "mid"]
        }
        expected_value = []
        actual_value = self.obj.validate_user_data(post_data=post_data)
        self.assertListEqual(actual_value, expected_value)

    def test_2(self):
        """
        test case when user name is less than 2 word
        """
        post_data = {
            "username": "t",
            "email": "nishant9233@test5.com",
            "password": "nishantran123",
            "confirm_password": "nishantran123",
            "tag": ["short", "mid"]
        }
        expected_value = ["Username needs to be more than 1 letter"]
        actual_value = self.obj.validate_user_data(post_data=post_data)
        self.assertListEqual(actual_value, expected_value)

    def test_3(self):
        post_data = {
            "username": "has",
            "email": "nishant9233@test5.com",
            "password": "nishantran123",
            "confirm_password": "nishantran123",
            "tag": ["shot", "mid"]
        }
        expected_value = ["tag must be a either of these : short, mid, long"]
        actual_value = self.obj.validate_user_data(post_data=post_data)
        self.assertListEqual(actual_value, expected_value)

    def test_4(self):
        post_data = {
            "username": "has",
            "email": "nishant9233@test5.com",
            "password": "nishantran123",
            "confirm_password": "nishantran23",
            "tag": ["short", "mid"]
        }
        expected_value = ["Your passwords don't match"]
        actual_value = self.obj.validate_user_data(post_data=post_data)
        self.assertListEqual(actual_value, expected_value)

    def test_5(self):
        post_data = {
            "username": "test",
            "email": "test@test.com",
            "password": "12345678",
            "confirm_password": "12345678",
            "tag": ["short", "mid"]
        }
        expected_value = ['Email already exists', 'Username already exists']
        actual_value = self.obj.validate_user_data(post_data=post_data)
        self.assertListEqual(actual_value, expected_value)


class TestPost(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()

    @patch("jwt.encode")
    @patch("quotes_app.views.user_register_view.UserRegister.validate_user_data")
    def test_1(self, user_data, token):
        user_data.return_value = []
        token.return_value = "abc"
        valid_payload = {
            "username": "testingalphaaa",
            "email": "nishant9233@test5.com",
            "password": "nishantran123",
            "confirm_password": "nishantran123",
            "tag": ["short", "mid"]
        }
        expected_response = {
            "status": "success",
            "message": "user created successfully",
            "token": "abc"
        }
        response = self.client.post(
            path="http://127.0.0.1:8000/register_user/",
            data=json.dumps(valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.data, expected_response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @patch("jwt.encode")
    @patch("quotes_app.views.user_register_view.UserRegister.validate_user_data")
    def test_2(self, user_data, token):
        user_data.return_value = ["Username needs to be more than 1 letter"]
        token.return_value = "abc"
        invalid_payload = {
            "username": "t",
            "email": "nishant9233@test5.com",
            "password": "nishantran123",
            "confirm_password": "nishantran123",
            "tag": ["short", "mid"]
        }
        expected_response = {
            'message': ['Username needs to be more than 1 letter'],
            'status': 'Failure'
        }
        response = self.client.post(
            path="http://127.0.0.1:8000/register_user/",
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.data, expected_response)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
