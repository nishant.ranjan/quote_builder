import datetime
import hashlib
import json
import uuid
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from rest_framework import status

from quotes_app.models import UserBase, Quotes
from rest_framework.test import APIClient

from quotes_app.views.quote_fetch_view import QuotesFetchView


class TestGet(TestCase):
    def setUp(self) -> None:
        secret_key = str(uuid.uuid4())
        expiry_date_time = datetime.datetime.now() + datetime.timedelta(
            minutes=settings.TOKEN_EXPIRY_DURATION)
        UserBase.objects.create(
            username="test", email="test@test.com",
            password=hashlib.sha512("12345678".encode('utf-8')).hexdigest(),
            tag=str(["short", "mid"]),
            token_expiry_date=expiry_date_time,
            secret_key=secret_key
        )
        self.client = APIClient()

    @patch("quotes_app.views.quote_fetch_view.validate_token")
    @patch("quotes_app.views.quote_fetch_view.QuotesFetchView.get_quote")
    def test_1(self, quote, token_valid):
        token_valid.return_value = True
        valid_payload = {
            "username": "test",
            "password": "12345678",
            "token": "abc"
        }

        quote.return_value = {
            "quote": "test the data",
            "tag": "mid"
        }
        expected_response = {
            "status": "Success",
            "data": {
                "quote": "test the data",
                "tag": "mid"
            }
        }
        response = self.client.post(
            path="http://127.0.0.1:8000/quotes/random/",
            data=json.dumps(valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.data, expected_response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch("quotes_app.views.quote_fetch_view.validate_token")
    @patch("quotes_app.views.quote_fetch_view.QuotesFetchView.get_quote")
    def test_2(self, quote, token_valid):
        token_valid.return_value = False
        valid_payload = {
            "username": "test",
            "password": "12345678",
            "token": "abc"
        }

        quote.return_value = {
            "quote": "test the data",
            "tag": "mid"
        }
        expected_response = {
            "status": "Failure",
            "error": "invalid token or invalid credentials provided"
        }
        response = self.client.post(
            path="http://127.0.0.1:8000/quotes/random/",
            data=json.dumps(valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.data, expected_response)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class TestGetQuote(TestGet):
    def setUp(self) -> None:
        Quotes.objects.create(quote="testing the quote in here", tag="mid")
        self.obj = QuotesFetchView()

    @patch("random.choice")
    def test_1(self, random):
        random.return_value = {"quote": "testing the quote in here", "tag": "mid"}
        expected_value = {"quote": "testing the quote in here", "tag": "mid"}
        actual_value = self.obj.get_quote(["mid"])
        self.assertDictEqual(actual_value, expected_value)
