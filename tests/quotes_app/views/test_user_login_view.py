import datetime
import hashlib
import json
import uuid
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from rest_framework import status

from rest_framework.test import APIClient

from quotes_app.models import UserBase
from quotes_app.views.user_login_view import UserLogin


class TestPost(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        secret_key = str(uuid.uuid4())
        expiry_date_time = datetime.datetime.now() + datetime.timedelta(
            minutes=settings.TOKEN_EXPIRY_DURATION)
        UserBase.objects.create(
            username="test", email="test@test.com",
            password=hashlib.sha512("12345678".encode('utf-8')).hexdigest(),
            tag=str(["short", "mid"]),
            token_expiry_date=expiry_date_time,
            secret_key=secret_key
        )

    @patch("quotes_app.views.user_login_view.UserLogin.update_token_in_db")
    def test_1(self, user_data):
        user_data.return_value = {
            "token": "abc"
        }
        valid_payload = {
            "username": "test",
            "password": "12345678"
        }
        expected_response = user_data.return_value
        response = self.client.post(
            path="http://127.0.0.1:8000/refresh_token/",
            data=json.dumps(valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.data, expected_response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_2(self):
        invalid_payload = {
            "username": "test1",
            "password": "12345678"
        }
        expected_response = {
                    'error': 'Can not authenticate with the given credentials or the account '
                             'has been deactivated'
                }
        response = self.client.post(
            path="http://127.0.0.1:8000/refresh_token/",
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.data, expected_response)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @patch("quotes_app.views.user_login_view.UserLogin.update_token_in_db")
    def test_3(self, user_data):
        user_data.return_value = {}
        valid_payload = {
            "username": "test",
            "password": "12345678"
        }
        expected_response = {
                        'error': 'unable to update db'
                    }
        response = self.client.post(
            path="http://127.0.0.1:8000/refresh_token/",
            data=json.dumps(valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.data, expected_response)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)


class TestUpdateTokenInDB(TestCase):
    def setUp(self) -> None:
        secret_key = str(uuid.uuid4())
        expiry_date_time = datetime.datetime.now() + datetime.timedelta(
            minutes=settings.TOKEN_EXPIRY_DURATION)
        UserBase.objects.create(
            username="test", email="test@test.com",
            password=hashlib.sha512("12345678".encode('utf-8')).hexdigest(),
            tag=str(["short", "mid"]),
            token_expiry_date=expiry_date_time,
            secret_key=secret_key
        )
        self.obj = UserLogin()

    @patch("jwt.encode")
    def test_1(self, token):
        """
        test case when all the data is correct
        """
        token.return_value = "abc"
        user = {
            "username": "test",
            "password": "12345678",
        }
        expected_value = {
            "token": token.return_value
        }
        actual_value = self.obj.update_token_in_db(user=user)
        self.assertDictEqual(actual_value, expected_value)

    def test_2(self):
        """
        test case when all the data is correct
        """
        user = {
            "username": "test1",
            "password": "12345678",
        }
        expected_value = {}
        actual_value = self.obj.update_token_in_db(user=user)
        self.assertDictEqual(actual_value, expected_value)
