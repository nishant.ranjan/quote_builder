# Register your models here.
from django.contrib import admin
from quotes_app.models import ProjectBase, UserBase

admin.site.register(ProjectBase)
admin.site.register(UserBase)
