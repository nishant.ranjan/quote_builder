from django.db import models


class ProjectBase(models.Model):
    objects = models.Manager()
    created_dt = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)


class UserBase(ProjectBase):
    username = models.CharField(max_length=45, blank=False, null=False)
    email = models.CharField(max_length=45, blank=False, null=False)
    password = models.CharField(max_length=500, blank=False, null=False)
    tag = models.CharField(max_length=30)
    token_expiry_date = models.DateTimeField()
    secret_key = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.username, self.email


class Quotes(ProjectBase):
    tag_choices = (("S", "short"),
                   ("M", "mid"),
                   ("L", "long"))
    quote = models.TextField(max_length=1000)
    tag = models.CharField(max_length=10, choices=tag_choices)
