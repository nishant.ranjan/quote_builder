import datetime
import hashlib
import re

import uuid
import jwt
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from quotes_app.models import UserBase
Name_Regex = re.compile(r'^[A-Za-z ]+$')


class UserRegister(APIView):

    @staticmethod
    def validate_user_data(post_data):
        errors = []
        tag_list = ["short", "mid", "long"]
        if len(post_data) != 5:
            errors.append("Please pass all the specified arguments: name, email, password, "
                          "confirm_password and tag")
            return errors
        if not isinstance(post_data["tag"], list) or len(post_data["tag"]) > 3 \
                or len(post_data["tag"]) == 0:
            errors.append("tag must be a list also it cant be empty or more than 3 values")
        else:
            for tag in post_data["tag"]:
                if tag not in tag_list:
                    errors.append("tag must be a either of these : short, mid, long")
                    break
        if len(post_data['username']) < 2:
            errors.append("Username needs to be more than 1 letter")
        if not Name_Regex.match(post_data['username']):
            errors.append("username can only be letters")
        if len(UserBase.objects.filter(email=post_data['email'])) > 0:
            errors.append("Email already exists")
        if len(UserBase.objects.filter(username=post_data['username'])) > 0:
            errors.append("Username already exists")
        if len(post_data["email"]) == 0:
            errors.append("Please insert an email address")
        elif not re.search(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+.[a-zA-Z]+$', post_data["email"]):
            errors.append("Please insert a valid email address")
        if post_data['password'] != post_data.get('confirm_password'):
            errors.append("Your passwords don't match")
        if len(post_data['password']) < 8:
            errors.append("Password needs to be more than 8 letters")
        return errors

    def post(self, request):
        response_dict = {}
        try:
            valid = self.validate_user_data(request.data)
            if not valid:

                secret_key = str(uuid.uuid4())
                expiry_date_time = datetime.datetime.now() + datetime.timedelta(
                    minutes=settings.TOKEN_EXPIRY_DURATION)
                UserBase.objects.create(username=request.data['username'], email=request.data[
                    'email'], password=hashlib.sha512(request.data["password"].encode('utf-8'))
                                        .hexdigest(), tag=str(request.data["tag"]),
                                        token_expiry_date=expiry_date_time, secret_key=secret_key)
                user = {
                    "username": request.data['username'],
                    "password": request.data['password']
                }
                token = jwt.encode(user, settings.SECRET_KEY)
                response_dict.update({
                    "status": "success",
                    "message": "user created successfully",
                    "token": token
                })
                return Response(response_dict, status=status.HTTP_201_CREATED)
            else:
                response_dict.update({
                    "status": "Failure",
                    "message": valid
                })
                return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            response_dict["error"] = "invalid key provided in input"
            return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            response_dict["error"] = str(e)
            return Response(response_dict, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
