import ast
import logging
import random

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from quotes_app.models import UserBase, Quotes
from quotes_app.utils import validate_token

logger = logging.getLogger(__name__)

logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")

file_handler = logging.FileHandler('fetch_quote.log', mode='w')
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)


class QuotesFetchView(APIView):

    def post(self, request):
        response_dict = {}
        try:
            token_valid = validate_token(request)
            if token_valid:
                logger.info("token validated successfully")
                tag = request.data.get("tag", "")
                if tag and isinstance(tag, list):
                    tag_list = tag
                    logger.info("tag provided by user")
                else:
                    logger.info("tag not provided by user")
                    user_tag = UserBase.objects.filter(username=request.data["username"]).values(
                        "tag")
                    logger.info(f"successfully fetched tag for user: {request.data['username']}")

                    tag_list = ast.literal_eval(list(user_tag)[0]['tag'])
                quote = self.get_quote(tag_list)
                if quote:
                    logger.info(f"successfully fetched quote for user: {request.data['username']}")
                    response_dict["status"] = "Success"
                    response_dict["data"] = quote
                    return Response(response_dict, status=status.HTTP_200_OK)
                else:
                    response_dict["status"] = "Failure"
                    response_dict["data"] = "no quotes found in db"
                    return Response(response_dict, status=status.HTTP_200_OK)
            else:
                logger.info(f"invalid token provided")
                response_dict["status"] = "Failure"
                response_dict["error"] = "invalid token or invalid credentials provided"
                return Response(response_dict, status=status.HTTP_401_UNAUTHORIZED)
        except KeyError:
            response_dict["status"] = "Failure"
            response_dict["error"] = "username or password not provided"
            return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error(f"some error happened error: {e}")
            response_dict["status"] = "Failure"
            response_dict["error"] = str(e)
            return Response(response_dict, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @staticmethod
    def get_quote(tag_list):
        try:
            quotes = Quotes.objects.filter(tag__in=tag_list).values("quote", "tag")
            if quotes:
                return random.choice(list(quotes))
            else:
                return []
        except ConnectionError as e:
            logger.error(f"some error happened due to error: {e}")
            return []
        except Exception as e:
            logger.error(f"some error happened due to error: {e}")
            return []
