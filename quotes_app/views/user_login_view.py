import datetime
import hashlib

import uuid
import jwt
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from quotes_app.models import UserBase


class UserLogin(APIView):

    def post(self, request):
        try:
            username = request.data['username']
            password = request.data['password']
            user = {
                "username": username,
                "password": password
            }
            user_data = UserBase.objects.filter(username=username, password=hashlib.sha512(
                request.data["password"].encode('utf-8')).hexdigest()).values("username")
            if user_data:
                user_details = self.update_token_in_db(user)
                if user_details:
                    return Response(user_details, status=status.HTTP_200_OK)
                else:
                    response_dict = {
                        'error': 'unable to update db'
                    }
                    return Response(response_dict, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            else:
                response_dict = {
                    'error': 'Can not authenticate with the given credentials or the account '
                             'has been deactivated'
                }
                return Response(response_dict, status=status.HTTP_403_FORBIDDEN)
        except KeyError:
            response_dict = {'error': 'please provide a email and a password'}
            return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            res = {'error': str(e)}
            return Response(res, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @staticmethod
    def update_token_in_db(user):
        """
        @param user:
        @return:
        """
        secret_key = str(uuid.uuid4())
        expiry_date_time = datetime.datetime.now() + datetime.timedelta(
            minutes=settings.TOKEN_EXPIRY_DURATION)
        db_updated = UserBase.objects.filter(username=user["username"]).update(secret_key=secret_key,
                                                                               token_expiry_date=expiry_date_time)
        if db_updated:
            token = jwt.encode(user, secret_key)
            user_details = {'token': token}
            return user_details
        return {}
