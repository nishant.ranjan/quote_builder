import datetime

import jwt

from quotes_app.models import UserBase


def validate_token(post_data):
    try:
        user = {
            "username": post_data.data['username'],
            "password": post_data.data['password']
        }
        expiry_data = UserBase.objects.filter(username=post_data.data["username"],
                                              token_expiry_date__gte=datetime.datetime.now()).\
            values("secret_key", "token_expiry_date")
        if expiry_data:
            secret_key = expiry_data[0]["secret_key"]
            token = jwt.encode(user, secret_key)
            if token.decode(encoding='UTF-8') == post_data.data.get("token"):
                return True
        return False
    except KeyError as e:
        raise KeyError(e)
    except Exception as e:
        raise e
