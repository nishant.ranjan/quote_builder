"""quote_builder URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from quotes_app.views.quote_fetch_view import QuotesFetchView
from quotes_app.views.user_login_view import UserLogin
from quotes_app.views.user_register_view import UserRegister

urlpatterns = [
    # Your other URL entries.
    path('admin/', admin.site.urls),
    path('register_user/', UserRegister.as_view(), name='user_registration'),
    path('quotes/random/', QuotesFetchView.as_view(), name="quote_fetch"),
    path('refresh_token/', UserLogin.as_view(), name="refreshes_token"),
]
